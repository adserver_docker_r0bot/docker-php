FROM ubuntu:16.04

COPY ./docker-entrypoint.sh /opt/php-setup/docker-entrypoint.sh

ENV PHP_HOME /home/php
ENV PHP_WORKDIR /app

RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        gettext \
        unzip \
        libpcre3-dev \
        openssl \
        git-core \
        language-pack-en-base \
        openssh-client \
        software-properties-common \
        nmap \
        net-tools \
        iputils-ping \
        wget \
        gosu \
    && LC_ALL=en_US.UTF-8 apt-add-repository ppa:ondrej/php \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        imagemagick \
        php7.2 \
        php7.2-apcu \
        php7.2-bz2 \
        php7.2-cli \
        php7.2-common \
        php7.2-curl \
        php7.2-dev \
        php7.2-fpm \
        php7.2-fileinfo \
        php7.2-gd \
        php7.2-intl \
        php7.2-json \
        php7.2-mbstring \
        php7.2-mysql \
        php7.2-xml \
        php7.2-zip \
        php7.2-xdebug \
        php7.2-bcmath \
        php-memcache \
        php-memcached \
        php-amqp \
        php-apcu \
        php-imagick \
        php-redis \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && gosu nobody true \
    && mkdir -p /run/php \
    && chmod +x /opt/php-setup/docker-entrypoint.sh

# Configure PHP
RUN set -x \
    && sed -i 's/user\ =\ www-data/user\ =\ php/g' /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i 's/group\ =\ www-data/group\ =\ php/g' /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i 's/listen.owner\ =\ www-data/listen.owner\ =\ php/g' /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i 's/listen.group\ =\ www-data/listen.group\ =\ php/g' /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i 's/listen\ =\ \/run\/php\/php7.2-fpm\.sock/listen\ =\ [::]:9000/g' /etc/php/7.2/fpm/pool.d/www.conf \
    && sed -i 's/;daemonize\ =\ yes/daemonize\ =\ no/g' /etc/php/7.2/fpm/php-fpm.conf \
    && echo "opcache.enable = 1" >> /etc/php/7.2/mods-available/opcache.ini \
    && echo "opcache.fast_shutdown = 1" >> /etc/php/7.2/mods-available/opcache.ini \
    && echo "opcache.interned_strings_buffer = 16" >> /etc/php/7.2/mods-available/opcache.ini \
    && echo "opcache.max_accelerated_files = 7963" >> /etc/php/7.2/mods-available/opcache.ini \
    && echo "opcache.memory_consumption = 128" >> /etc/php/7.2/mods-available/opcache.ini \
    && echo "opcache.revalidate_freq = 0" >> /etc/php/7.2/mods-available/opcache.ini \
    && echo "xdebug.remote_host=172.20.0.1" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_port=9111" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_handler=dbgp" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_enable=1" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_autostart=1" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_connect_back=0" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.idekey=\"docker\"" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.profiler_output_dir=/usr/share/nginx/html/xdebug" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.profiler_enable=0" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.profiler_enable_trigger=1" >> /etc/php/7.2/mods-available/xdebug.ini

EXPOSE 9000
EXPOSE 9111
ENTRYPOINT ["/opt/php-setup/docker-entrypoint.sh"]
CMD ["/usr/sbin/php-fpm7.2"]
